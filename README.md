# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is to demonstrate low-level camera functionality from Xamarin.Forms. This version supports only iOS though.
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Requires JetBrains.Annotations (for clean view model implementation)
* Just download code and build/run it.

### Contribution guidelines ###

* This version uses iOS AVFoundation (iOS) only. Feel free to suggest an Android implementation!

### Who do I talk to? ###

* Jonas Rembratt