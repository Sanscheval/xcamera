﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Sanscheval.XCamera.Models;
using Xamarin.Forms;

namespace Sanscheval.XCamera
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((MainPageVM)BindingContext).InitializeCamera().WithPreview(Preview);
            Photo.HeightRequest = Preview.Height;
            Photo.WidthRequest = Preview.Width;
        }

        private void ViewModel_OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var vm = (MainPageVM) BindingContext;
            switch (e.PropertyName)
            {
                case "PhotoSource":
                    if (!vm.IsPhotoZoomed)
                        unzoomPhoto();
                    break;

                case "IsPhotoZoomed":
                    if (vm.IsPhotoZoomed)
                        zoomPhoto();
                    else
                        unzoomPhoto();
                    break;
            }
        }

        private async void unzoomPhoto()
        {
            Photo.IsVisible = true;
            await Task.WhenAll(
                Photo.ScaleTo(0.4),
                Photo.TranslateTo(Preview.Width * 0.3, Preview.Height * 0.3)
            );
        }

        private async void zoomPhoto()
        {
            Photo.IsVisible = true;
            await Task.WhenAll(
                Photo.ScaleTo(1),
                Photo.TranslateTo(0, 0)
            );
        }

    }
}
