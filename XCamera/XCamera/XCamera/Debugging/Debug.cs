﻿using System.Reflection;

namespace Sanscheval.XCamera.Debugging
{
    class Debug
    {
        public static void WriteNamesOfEmbeddedResources()
        {
            var assembly = typeof(App).GetTypeInfo().Assembly;
            foreach (var res in assembly.GetManifestResourceNames())
            {
                System.Diagnostics.Debug.WriteLine(">>> " + res);
            }
        }
    }
}
