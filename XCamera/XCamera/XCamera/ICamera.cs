﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sanscheval.XCamera
{
    public interface ICamera
    {
        bool IsFunctional { get; }
        FlashMode Flash { get; set; }
        CameraPosition Position { get; set; }
        bool HDR { get; set; }
        ICamera WithPreview(Page page);
        ICamera WithPreview(View view);
        Task<byte[]> TakePhoto();
    }

    public enum FlashMode { Off, On, Auto }

    public enum CameraPosition { Front, Back }
}