﻿using System;
using System.IO;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sanscheval.XCamera.Models
{
    class MainPageVM : ViewModel
    {
        private ImageSource _flashImageSource;
        private ImageSource _cameraPositionImageSource;
        private ImageSource _takePhotoImageSource;
        private ImageSource _hdrStatusImageSource;
        private ImageSource _photoSource;
        private ICommand _flashCommand;
        private ICamera _camera;
        private string _photoSizeText;
        private bool _isPhotoZoomed;

        public ImageSource PhotoSource
        {
            get { return _photoSource; }
            set { _photoSource = value; OnPropertyChanged(); }
        }

        public bool IsPhotoZoomed
        {
            get { return _isPhotoZoomed; }
            set { _isPhotoZoomed = value; OnPropertyChanged(); }
        }

        public ImageSource FlashImageSource
        {
            get { return _flashImageSource; }
            set { _flashImageSource = value; OnPropertyChanged(); }
        }

        public ImageSource CameraPositionImageSource
        {
            get { return _cameraPositionImageSource; }
            set { _cameraPositionImageSource = value; OnPropertyChanged(); }
        }

        public ImageSource TakePhotoImageSource
        {
            get { return _takePhotoImageSource; }
            set { _takePhotoImageSource = value; OnPropertyChanged(); }
        }

        public ImageSource HDRStatusImageSource
        {
            get { return _hdrStatusImageSource; }
            set { _hdrStatusImageSource = value; OnPropertyChanged(); }
        }

        public string PhotoSizeText
        {
            get { return _photoSizeText; }
            set { _photoSizeText = value; OnPropertyChanged(); }
        }

        public ICommand FlashCommand
        {
            get { return _flashCommand; }
            set { _flashCommand = value; OnPropertyChanged(); }
        }

        public ICommand CameraPositionCommand { get; }
        public ICommand TakePhotoCommand { get; }
        public ICommand HDRCommand { get; }
        public ICommand PhotoTappedCommand { get; }


        private void onSetFlash()
        {
            if (!_camera.IsFunctional) return;
            switch (_camera.Flash)
            {
                case FlashMode.Off:
                    _camera.Flash = FlashMode.Auto;
                    break;
                case FlashMode.On:
                    _camera.Flash = FlashMode.Off;
                    break;
                case FlashMode.Auto:
                    _camera.Flash = FlashMode.On;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            updateFlashIcon();
        }

        private void updateFlashIcon()
        {
            // todo Update flash icon
        }

        private void onSetCameraPosition()
        {
            if (!_camera.IsFunctional) return;
            _camera.Position = _camera.Position == CameraPosition.Back
                ? CameraPosition.Front
                : CameraPosition.Back;
        }

        private async void onTakePhoto()
        {
            if (!_camera.IsFunctional) return;
            var data = await _camera.TakePhoto();
            if (data != null)
            {
                PhotoSource = ImageSource.FromStream(() => new MemoryStream(data));
                PhotoSizeText = $"{data.Length / 1024.0:####.##}Kb";
                IsPhotoZoomed = false;
                return;
            }
            PhotoSizeText = "";
        }

        private void onToggleHdr()
        {
            if (!_camera.IsFunctional) return;
            _camera.HDR = !_camera.HDR;
            HDRStatusImageSource = ImageSource.FromResource(resource(_camera.HDR ? "HDRon.png" : "HDRoff.png"));
        }

        private void onPhotoTapped() => IsPhotoZoomed = !IsPhotoZoomed;

        public MainPageVM InitializeCamera()
        {
            _camera = DependencyService.Get<ICamera>();
            HDRStatusImageSource = ImageSource.FromResource(resource(_camera.HDR ? "HDRon.png" : "HDRoff.png"));
            return this;
        }

        public MainPageVM WithPreview(View view)
        {
            if (_camera.IsFunctional)
                _camera.WithPreview(view);
            return this;
        }

        public MainPageVM WithPreview(Page page)
        {
            if (_camera.IsFunctional)
                _camera.WithPreview(page);
            return this;
        }


        public MainPageVM()
        {
            FlashImageSource = ImageSource.FromResource(resource("FlashButton.png"));
            CameraPositionImageSource = ImageSource.FromResource(resource("ToggleCameraButton.png"));
            TakePhotoImageSource = ImageSource.FromResource(resource("TakePhotoButton.png"));
            HDRStatusImageSource = ImageSource.FromResource(resource("HDRoff.png"));
            FlashCommand = new Command(onSetFlash);
            CameraPositionCommand = new Command(onSetCameraPosition);
            TakePhotoCommand = new Command(onTakePhoto);
            HDRCommand = new Command(onToggleHdr);
            PhotoTappedCommand = new Command(onPhotoTapped);
        }


        private static string resource(string name)
        {
            return $"{typeof(App).Namespace}.Resources.{name}";
        }
    }
}
