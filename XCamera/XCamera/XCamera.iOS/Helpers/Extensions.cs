﻿using UIKit;
using Xamarin.Forms.Platform.iOS;

namespace Sanscheval.XCamera.iOS.Helpers
{
    static class Extensions
    {
        public static UIView GetUiView(this Xamarin.Forms.Page page)
        {
            var renderer = Platform.GetRenderer(page);
            if (renderer != null)
                return renderer.NativeView;

            renderer = Platform.CreateRenderer(page);
            Platform.SetRenderer(page, renderer);
            return renderer.NativeView;
        }

        public static UIView GetUiView(this Xamarin.Forms.View view)
        {
            var renderer = Platform.GetRenderer(view);
            if (renderer != null)
                return renderer.NativeView;

            renderer = Platform.CreateRenderer(view);
            Platform.SetRenderer(view, renderer);
            return renderer.NativeView;
        }
    }
}