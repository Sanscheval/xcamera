﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AVFoundation;
using Foundation;
using Sanscheval.XCamera.iOS;
using Sanscheval.XCamera.iOS.Helpers;
using UIKit;
using Xamarin.Forms;

[assembly:Dependency(typeof(IosCamera))]

namespace Sanscheval.XCamera.iOS
{
    class IosCamera : ICamera
    {
        private AVCaptureSession _captureSession;
        private AVCaptureDeviceInput _captureDeviceInput;
        private AVCaptureStillImageOutput _stillImageOutput;
        private CameraPosition _cameraPosition;
        private FlashMode _flashMode;
        private AVCaptureVideoPreviewLayer _videoPreviewLayer;
        private bool _hdr;

        public bool IsFunctional { get; private set; }

        public FlashMode Flash
        {
            get { return _flashMode; }
            set
            {
                if (value == _flashMode) return;
                _flashMode = value;
                setFlash();
            }
        }

        public CameraPosition Position
        {
            get { return _cameraPosition; }
            set
            {
                if (value == _cameraPosition) return;
                _cameraPosition = value;
                setCameraPosition();
            }
        }

        public bool HDR
        {
            get { return _hdr; }
            set { setHDR(value); }
        }

        public static bool IsAuthorized => AVCaptureDevice.GetAuthorizationStatus(AVMediaType.Video) == AVAuthorizationStatus.Authorized;

        public static async Task<bool> AuthorizeAsync()
        {
            if (IsAuthorized)
                return true;
            try
            {
                return await AVCaptureDevice.RequestAccessForMediaTypeAsync(AVMediaType.Video);
            }
            catch
            {
                return false;
            }
        }

        public async Task<byte[]> TakePhoto()
        {
            try
            {
                if (!await AuthorizeAsync()) return null;
                var connection = _stillImageOutput.ConnectionFromMediaType(AVMediaType.Video);
                var buffer = await _stillImageOutput.CaptureStillImageTaskAsync(connection);
                return AVCaptureStillImageOutput.JpegStillToNSData(buffer).ToArray();
            }
            catch (Exception e)
            {
                log(e);
                return null;
            }
        }

        private async void setCameraPosition()
        {
            try
            {
                if (!await AuthorizeAsync()) return;
                var position = _captureDeviceInput.Device.Position;
                position = position == AVCaptureDevicePosition.Front
                    ? AVCaptureDevicePosition.Back
                    : AVCaptureDevicePosition.Front;
                var device = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video).FirstOrDefault(d => d.Position == position);
                if (device == null)
                    return;

                configureCameraForDevice(device);
                _captureSession.BeginConfiguration();
                _captureSession.RemoveInput(_captureDeviceInput);
                _captureDeviceInput = AVCaptureDeviceInput.FromDevice(device);
                _captureSession.AddInput(_captureDeviceInput);
                _captureSession.CommitConfiguration();
            }
            catch (Exception ex)
            {
                log(ex);
            }
        }

        private async void setHDR(bool value)
        {
            try
            {
                if (!await AuthorizeAsync()) return;
                var device = _captureDeviceInput.Device;
                if (!device.AutomaticallyAdjustsVideoHdrEnabled)
                    return;

                device.LockForConfiguration(out NSError _);
                try
                {
                    device.VideoHdrEnabled = _hdr = value;
                }
                finally
                {
                    device.UnlockForConfiguration();
                }
            }
            catch (Exception ex)
            {
                log(ex);
            }
        }


        void configureCameraForDevice(AVCaptureDevice device)
        {
            if (device.IsFocusModeSupported(AVCaptureFocusMode.ContinuousAutoFocus))
            {
                device.LockForConfiguration(out NSError _);
                device.FocusMode = AVCaptureFocusMode.ContinuousAutoFocus;
                device.UnlockForConfiguration();
            }
            else if (device.IsExposureModeSupported(AVCaptureExposureMode.ContinuousAutoExposure))
            {
                device.LockForConfiguration(out NSError _);
                device.ExposureMode = AVCaptureExposureMode.ContinuousAutoExposure;
                device.UnlockForConfiguration();
            }
            else if (device.IsWhiteBalanceModeSupported(AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance))
            {
                device.LockForConfiguration(out NSError _);
                device.WhiteBalanceMode = AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance;
                device.UnlockForConfiguration();
            }
        }

        private void setFlash()
        {
            var device = _captureDeviceInput.Device;
            if (!device.HasFlash)
                return;

            device.LockForConfiguration(out NSError _);
            try
            {
                switch (Flash)
                {
                    case FlashMode.Auto:
                        device.FlashMode = AVCaptureFlashMode.Auto;
                        break;
                    case FlashMode.On:
                        device.FlashMode = AVCaptureFlashMode.On;
                        break;
                    case FlashMode.Off:
                        device.FlashMode = AVCaptureFlashMode.Off;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(Flash), Flash, null);
                }
            }
            catch (Exception ex)
            {
                log(ex);
            }
            finally
            {
                device.UnlockForConfiguration();
            }
        }

        private void log(Exception exception)
        {
            // todo Add log errors to IosCamera
        }

        private static void log(string message)
        {
            // todo Add log text messages to IosCamera
        }

        public static async Task<IosCamera> CreateAsync()
        {
            if (!await AuthorizeAsync())
            {
                log("Camera service cannot be provided without authorization.");
                return null;
            }
            return new IosCamera();
        }

        private async void setupLiveCameraStream()
        {
            try
            {
                if (!await AuthorizeAsync())
                {
                    IsFunctional = false;
                    return;
                }
                _captureSession = new AVCaptureSession();
                var captureDevice = AVCaptureDevice.GetDefaultDevice(AVMediaType.Video);
                configureCameraForDevice(captureDevice);
                _captureDeviceInput = AVCaptureDeviceInput.FromDevice(captureDevice);
                _captureSession.AddInput(_captureDeviceInput);
                _flashMode = getFlashModeFromDevice();
                _stillImageOutput = new AVCaptureStillImageOutput
                {
                    OutputSettings = new NSDictionary()
                };

                _captureSession.AddOutput(_stillImageOutput);
                _captureSession.StartRunning();
                IsFunctional = true;
            }
            catch (Exception ex)
            {
                log(ex);
                IsFunctional = false;
            }
        }

        public ICamera WithPreview(View view)
        {
            return WithPreview(view.GetUiView());
        }

        public ICamera WithPreview(Page page)
        {
            return WithPreview(page.GetUiView());
        }

        protected IosCamera WithPreview(UIView view)
        {
            if (view == null) return this;
            _videoPreviewLayer = new AVCaptureVideoPreviewLayer(_captureSession)
            {
                Frame = view.Frame
            };
            view.Layer.AddSublayer(_videoPreviewLayer);
            return this;
        }

        private FlashMode getFlashModeFromDevice()
        {
            switch (_captureDeviceInput.Device.FlashMode)
            {
                case AVCaptureFlashMode.Off:
                    return FlashMode.Off;

                case AVCaptureFlashMode.On:
                    return FlashMode.On;

                case AVCaptureFlashMode.Auto:
                    return FlashMode.Auto;

                default:
                    throw new NotSupportedException();
            }
        }

        public IosCamera()
        {
            setupLiveCameraStream();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_captureSession == null)
                    return;

                _captureSession.StopRunning();
                _captureSession.Dispose();
                _captureSession = null;
                _captureDeviceInput.Dispose();
                _captureDeviceInput = null;
                _stillImageOutput.Dispose();
                _stillImageOutput = null;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}